# clamav

Forked version of clamav with ClearOS changes applied

* git clone git@gitlab.com:clearos/upstream/epel/clamav.git
* cd clamav
* git checkout epel7
* git remote add upstream https://src.fedoraproject.org/rpms/clamav.git
* git pull upstream epel7
* git checkout clear7
* git merge --no-commit epel7
* git commit

As clamav.net is now behind a Cloudflare proxy, the signatures and source file have to be downloaded to Koji into `/mnt/koji/source/clamav` and are referenced in the sources-clearos file. If you are building locally, you will need to download these yourself to your own repo. Gitlab cannot build the rpm's either so the .gitlab-ci.yml file has been removed.

The signatures can currently be downloaded from https://database.clamav.net/main.cvd, https://database.clamav.net/daily.cvd and https://database.clamav.net/bytecode.cvd. The sources can be downloaded from https://www.clamav.net/downloads.
